from __future__ import unicode_literals

from django.apps import AppConfig


class CustomBlogConfig(AppConfig):
    name = 'custom_blog'
