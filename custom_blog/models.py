from __future__ import unicode_literals

from django.db import models

from zinnia.models_bases.entry import AbstractEntry
from zinnia.models_bases import load_model_class


class BlogEntry(AbstractEntry):
    """docstring for CmpEntry"""
    team_reviews = models.ManyToManyField('cmp.Team',related_name="blog_reviewers")
    #review_obj = ReviewsManager()
    def __str__(self):
        return 'BlogEntry %s' % self.title

    class Meta(AbstractEntry.Meta):
        abstract = True



        