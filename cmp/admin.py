
from django.contrib import admin
from django.utils.translation import ungettext_lazy
from django.utils.translation import ugettext_lazy as _


from zinnia.models.entry import Entry
from zinnia.models.category import Category
from zinnia.admin.entry import EntryAdmin
from zinnia.admin.category import CategoryAdmin
from zinnia.settings import ENTRY_BASE_MODEL
from .models import TeamCompetition, Team,TeamMember, MemberPoints


#if ENTRY_BASE_MODEL == 'zinnia.models_bases.entry.AbstractEntry':
#admin.site.unregister(Entry)

class UserEntryAdmin(EntryAdmin):
    def get_queryset(self, request):
        qs = super(UserEntryAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(authors=request.user)
    fieldsets = (
        (_('Content'), {
            'fields': (('title', 'status'), 'lead', 'content')}),
        (_('Illustration'), {
            'fields': ('image', 'image_caption'),
            'classes': ('collapse', 'collapse-closed')}),
        (_('Publication'), {
            'fields': ('publication_date', 'sites',
                       ('start_publication', 'end_publication')),
            'classes': ('collapse', 'collapse-closed')}),
        (_('Discussions'), {
            'fields': ('comment_enabled', 'pingback_enabled',
                       'trackback_enabled'),
            'classes': ('collapse', 'collapse-closed')}),
        (_('Privacy'), {
            'fields': ('login_required', 'password'),
            'classes': ('collapse', 'collapse-closed')}),
        (_('Templates'), {
            'fields': ('content_template', 'detail_template'),
            'classes': ('collapse', 'collapse-closed')}),
        (_('Metadatas'), {
            'fields': ('featured', 'excerpt', 'authors', 'related'),
            'classes': ('collapse', 'collapse-closed')}),
        (None, {'fields': ('categories', 'tags', 'slug')}))


class TeamMemberInline(admin.TabularInline):
    model = TeamMember

class TeamAdmin(admin.ModelAdmin):
    inlines = [
        TeamMemberInline,
    ]
    list_display = ('name', 'competition','get_team_points')


class TeamPointAdmin(admin.ModelAdmin):
    list_display = ('task_details', 'point','participant','added_by','get_total_points')

admin.site.register(Entry, UserEntryAdmin)

admin.site.register(TeamCompetition)

admin.site.register(Team,TeamAdmin)

admin.site.register(TeamMember)

admin.site.register(MemberPoints,TeamPointAdmin)