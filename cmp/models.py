from __future__ import unicode_literals
import operator
from django.db import models
from django.contrib.auth.models import User
from zinnia.models.entry import Entry
from django.conf import settings
#from .managers import *

class TeamCompetition(models.Model):
    """docstring for ClassName"""
    name = models.CharField(max_length=30)
    active = models.BooleanField(default=True)
    def __str__(self):
        return self.name

class Team(models.Model):
    """docstring for ClassName"""
    name = models.CharField(max_length=30)
    competition = models.ForeignKey(TeamCompetition)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    @property
    def get_team_points(self):
        return sum([points.point for points in self.team_points.all()])

    @classmethod
    def get_team_ranking(cls):
        teams = cls.objects.filter(active=True,competition__active=True)
        team_points = dict([(team.name, team.get_team_points) for team in teams])
        team_points = sorted(team_points.items(), key=operator.itemgetter(1))
        team_points.reverse()
        return team_points


class TeamMember(models.Model):
    """docstring for ClassName"""
    team = models.ForeignKey(Team,related_name="members")
    member = models.ForeignKey(settings.AUTH_USER_MODEL,blank=True, null=True,related_name="teammembers")    
    is_leader = models.BooleanField(default=False)

    @property
    def get_points(self):
        return sum([ points.point for points in  self.member_points.all() ])
    

class MemberPoints(models.Model):
    """docstring for ClassName"""
    task_details = models.CharField(max_length=500)
    point = models.FloatField("Points",default=0.0)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL,blank=True, null=True,related_name="point_addeds")
    participant = models.ForeignKey(settings.AUTH_USER_MODEL,blank=True, null=True,related_name="points")
    team = models.ForeignKey(Team,blank=True, null=True,related_name="team_points")
    team_member = models.ForeignKey(TeamMember,blank=True, null=True,related_name="member_points")

    
    def __str__(self):
        return "%s-%s"%(self.task_details,str(self.point))

    def save(self, *args, **kwargs):
        if self.participant:
            self.team_member = TeamMember.objects.get(member = self.participant,team__active=True)
            self.team = self.team_member.team
        super(MemberPoints, self).save(*args, **kwargs)


    @classmethod
    def get_points_by_team(cls,team):
        team_points = sum([cls.get_points_by_user(member.member) for member in team.members.all()])
        return team_points



    @classmethod
    def get_points_by_user(cls,user):
        my_points = cls.objects.filter(participant=user)
        return sum([point.point for point in my_points])

    @property
    def get_total_points(self):
        return type(self).get_points_by_user(self.participant)



def get_user_points(self):
    if self.is_leader():
        return self.team().get_team_points
    team_member = self.teammembers.filter(team__active=True)
    if team_member:
        return team_member[0].get_points
    return False

def is_leader(self):
    try:
        team_member = self.teammembers.get(team__active=True,is_leader=True)
        return True
    except:
        return False

def team(self):
    team = self.teammembers.filter(team__active=True)[0].team
    return team


User.add_to_class("get_user_points",get_user_points)
User.add_to_class("is_leader",is_leader)
User.add_to_class("team",team)



STATUS_CHOICES = (("assigned", 'assigned'),
                  ("approved", 'approved'),
                  ("rejected", 'rejected')
                  )


class BlogReviews(models.Model):
    team = models.ForeignKey(Team,related_name="reviews")
    review_by = models.ForeignKey(settings.AUTH_USER_MODEL,blank=True, null=True,related_name="blogs1")
    status = models.IntegerField(('status1'),choices=STATUS_CHOICES, default='assigned')
    review_comment = models.CharField(max_length=500)
    blog = models.ForeignKey(Entry,related_name='reviews')

# from django.db.models.signals import post_save
# from django.dispatch import receiver
# @receiver(post_save, sender=Entry)
# def create_initial_story(sender,instance, signal, created, **kwargs):
#     print "helloooo!"
#     if created: