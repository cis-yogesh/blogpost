from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.db.models import Count,Sum
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from zinnia.models.entry import Entry
from .models import MemberPoints, TeamMember, Team


class DashbordView(LoginRequiredMixin,TemplateView):

    template_name = "dashbord/index.html"

    def get_context_data(self, **kwargs):
        context = super(DashbordView, self).get_context_data(**kwargs)
        user = self.request.user
        if user.is_superuser:
            context['teams'] = Team.objects.filter(active=True)
        return context

    def get_template_names(self):
        user = self.request.user
        if user.is_superuser:
            self.template_name = "manager/manager_index.html"
        return [self.template_name]




class TeamMembersView(LoginRequiredMixin,TemplateView):

    template_name = "dashbord/team_members.html"

class GameStatisticsView(LoginRequiredMixin,TemplateView):

    template_name = "dashbord/game_statistics.html"
    def get_context_data(self, **kwargs):
        context = super(GameStatisticsView, self).get_context_data(**kwargs)
        context['teams'] = Team.get_team_ranking()
        #context['teams'] = Team.get_team_ranking()
        context['top_scores'] = MemberPoints.objects.values('participant__username','team__name').annotate(sum_score=Sum('point')).order_by('-sum_score')[:5]
        return context



class BlogPostView(LoginRequiredMixin,CreateView):
    template_name = "dashbord/blog/create.html"
    model = Entry
    fields = ['title','lead','content','image','categories']


class BlogReviewViewListing(LoginRequiredMixin,View):
    """docstring for BlogReviewView"""
    template_name = "dashbord/blog/review_blog.html"

    def get(self, request):
        active_team_member = request.user.teammembers.filter(team__active=True)
        if active_team_member:
            team = active_team_member[0].team
        else:
            team = False
        
        blogs = Entry.objects.filter(status=0)
        ctx = {'blogs':blogs}
        return render(request, self.template_name,ctx)

class BlogReviewView(LoginRequiredMixin,View):
    """docstring for BlogReviewView"""
    template_name = "dashbord/blog/review_blog.html"

    def get(self, request, *args, **kwargs):
        #import pdb;pdb.set_trace()
        active_team_member = request.user.teammembers.filter(team__active=True)
        if active_team_member:
            team = active_team_member[0].team
        else:
            team = False
        
        blogs = Entry.objects.filter(status=0)
        ctx = {'blogs':blogs}
        return render(request, self.template_name,ctx)
