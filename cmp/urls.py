from django.conf.urls import url

from .views import (DashbordView,BlogReviewView, BlogReviewViewListing,
						TeamMembersView, BlogPostView,GameStatisticsView,
						)

urlpatterns = [
    url(r'^$', DashbordView.as_view(), name='dashbord'),
    url(r'^team-members/$', TeamMembersView.as_view(), name='team-memebers'),
    url(r'^game-statistics/$', GameStatisticsView.as_view(), name='game-statistics'),
    
    url(r'^blog-post/$', BlogPostView.as_view(), name='blog-post-new'),

    url(r'^blog-review/$', BlogReviewViewListing.as_view(), name='blog-review-listing'),
    url(r'^blog-review/(?P<pk>\d+)/$', BlogReviewView.as_view(), name='blog-review'),
]