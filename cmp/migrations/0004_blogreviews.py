# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-22 07:05
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('zinnia', '0004_on_delete_arg'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cmp', '0003_auto_20161220_1100'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlogReviews',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.IntegerField(choices=[('assigned', 'assigned'), ('approved', 'approved'), ('rejected', 'rejected')], default='assigned', verbose_name='status1')),
                ('review_comment', models.CharField(max_length=500)),
                ('blog', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reviews', to='zinnia.Entry')),
                ('review_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='blogs1', to=settings.AUTH_USER_MODEL)),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reviews', to='cmp.Team')),
            ],
        ),
    ]
