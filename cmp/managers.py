from django.db import models

class ReviewsManager(models.Manager):
    def pending_review(self):
        return self.filter(state='draft')
        
    def rejected(self):
        return self.filter(state='hidden')